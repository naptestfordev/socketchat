const express = require("express");
const app = express();
const server = require("http").createServer(app);
const io = require("socket.io")(server);
require("dotenv/config");

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

io.on("connection", (socket) => {
  console.log(`User with id ${socket.id} connected`);
  socket.on("message", (data) => {
    socket.broadcast.emit("message", data);
  });
});

const port = process.env.PORT || 5000;
server.listen(port, () => console.log(`Server is listening on port ${port}`));
